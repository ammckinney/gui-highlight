from xml.dom import minidom
import re

def xmlParse(file):
    
    # Parses the xml file to get node elements 
    nodes = minidom.parse(file).getElementsByTagName("node")
    coordinates = []

    # Saves the node bounds into a list if it is a leaf node 
    for node in nodes:
        if not node.hasChildNodes():
            numbers = [int(num) for num in re.findall(r'[\d.]+', node.getAttribute("bounds"))]
            coordinates.append(numbers)

    return(coordinates)