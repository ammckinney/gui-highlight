# How to Run 
- Clone into the repository 
- Navigate into the repository where main.py is 
- Compile and run python file with matching xml and png file as parameters: 
python main.py xml_file.xml png_file.png 
(If the xml and png files are not in the same directory as main.py use the full path name)