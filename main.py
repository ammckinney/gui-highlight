import sys
from parse import xmlParse
from draw import pngDraw

def main():

    # Retrieves xml and png file from command line 
    xmlFile = sys.argv[1]
    pngFile = sys.argv[2]

    coordinates = xmlParse(xmlFile)
    pngDraw(pngFile, coordinates)

if __name__ == "__main__":
    main()