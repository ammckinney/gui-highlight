from PIL import Image, ImageDraw
import os

def pngDraw(pngFile, coordinates):

    # Opens the image for drawing on 
    pngImage = Image.open(pngFile)

    # Draws rectangles corresponding to the list of coordinates 
    for coordinate in coordinates:
        drawImage = ImageDraw.Draw(pngImage)
        drawImage.rectangle(((coordinate[0], coordinate[1]), (coordinate[2], coordinate[3])), outline = "yellow", width = 8)

    # Retrieves the file name from the command line parameter
    splitName = pngFile.split(os.sep)
    filenameWithPng = splitName[len(splitName)-1]
    filenameWithoutPng = filenameWithPng[:-4]
    my_path = os.getcwd()

    # Saves output to output folder with _highlight and shows temporary version on screen 
    pngImage.save(my_path + os.sep + 'output' + os.sep + filenameWithoutPng + '_highlight' + '.png')
    pngImage.show()